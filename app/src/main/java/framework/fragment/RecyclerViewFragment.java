package framework.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.header.MaterialViewPagerHeaderDecorator;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import common.CommonConfigs;
import framework.TestRecyclerViewAdapter;
import liwen.zhao.gameScreenRecord.R;


/**
 * Created by florentchampigny on 24/04/15.
 */
public class RecyclerViewFragment extends Fragment {

    private static final boolean GRID_LAYOUT = false;

    File[] files;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    static RecyclerViewFragment recyclerViewFragment;

    public static RecyclerViewFragment newInstance() {
        if(recyclerViewFragment==null){
            recyclerViewFragment=new RecyclerViewFragment();
        }
        return recyclerViewFragment;
    }

    public void change() {
        refreshData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        //setup materialviewpager

        if (GRID_LAYOUT) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        mRecyclerView.setHasFixedSize(true);

        //Use this now
        mRecyclerView.addItemDecoration(new MaterialViewPagerHeaderDecorator());
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    private void refreshData() {
        initData();
        if(files!=null&&mRecyclerView!=null){
            mRecyclerView.setAdapter(new TestRecyclerViewAdapter(files));
        }
    }

    private void initData() {
        //打开指定目录
        File fileDir = new File(CommonConfigs.VIDEO_PATH);
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
        //取出文件列表：
        files= fileDir.listFiles();
    }
}
